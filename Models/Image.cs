﻿using System;
using System.Collections.Generic;

namespace ArtGallery.Models
{
    public partial class Image
    {
        public Image()
        {
            NewsNavigation = new HashSet<News>();
            Product = new HashSet<Product>();
            UserContent = new HashSet<UserContent>();
            Users = new HashSet<Users>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public int? Diyid { get; set; }
        public int? NewsId { get; set; }

        public virtual Diy Diy { get; set; }
        public virtual News News { get; set; }
        public virtual ICollection<News> NewsNavigation { get; set; }
        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<UserContent> UserContent { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
