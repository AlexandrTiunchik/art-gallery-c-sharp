﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace ArtGallery.Models
{
    public partial class News
    {
        public int Id { get; set; }
        //public int? ProfileId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        //public int? ImageId { get; set; }
        public string Author { get; set; }
        public string Date { get; set; }
        public string NewsType { get; set; }
        public List<string> NewsTag { get; set; }
        public string MainImage { get; set; }
        
        [JsonIgnore]
        public virtual List<Comment> Comments { get; set; }

        //public virtual Image ImageNavigation { get; set; }
        //public virtual Profile Profile { get; set; }
        //public virtual ICollection<Comment> Comment { get; set; }
        //public virtual ICollection<Image> Image { get; set; }
    }
}
