﻿using System;
using System.Collections.Generic;

namespace ArtGallery.Models
{
    public partial class Work
    {
        public int Id { get; set; }
        public int? ProfileId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public float Payment { get; set; }
        public string Currency { get; set; }
        public DateTime Date { get; set; }
        public string AdditionalTerms { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Category { get; set; }
        public string JobType { get; set; }

        public Profile Profile { get; set; }
    }
}
