﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ArtGallery.Models
{
    public partial class Comment
    {
        public int Id { get; set; }
        public string Message { get; set; }
        //public int? Diyid { get; set; }
        //public int? ProductId { get; set; }
        //public int? UserContentId { get; set; }
        
        public int NewsId { get; set; }
        
        [JsonIgnore]
        public News News { get; set; }
        
        public int ProfileId { get; set; }
        
        public Profile Profile { get; set; }
        

        //public virtual Diy Diy { get; set; }
        //public virtual News News { get; set; }
        //public virtual Product Product { get; set; }
        //public virtual Users User { get; set; }
        //public virtual UserContent UserContent { get; set; }
    }
}
