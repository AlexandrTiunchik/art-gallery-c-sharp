﻿using System;
using System.Collections.Generic;

namespace ArtGallery.Models
{
    public partial class UserContent
    {
        public UserContent()
        {
            Comment = new HashSet<Comment>();
        }

        public int Id { get; set; }
        public int? ProfileId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? ImageId { get; set; }
        public string ContentType { get; set; }
        public string ContentUrl { get; set; }

        public virtual Image Image { get; set; }
        public virtual Profile Profile { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
    }
}
