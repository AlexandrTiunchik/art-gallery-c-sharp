﻿using System;
using System.Collections.Generic;

namespace ArtGallery.Models
{
    public partial class Diy
    {
        public Diy()
        {
            Comment = new HashSet<Comment>();
            Image = new HashSet<Image>();
        }

        public int Id { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public int? ProfileId { get; set; }

        public virtual Profile Profile { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
        public virtual ICollection<Image> Image { get; set; }
    }
}
