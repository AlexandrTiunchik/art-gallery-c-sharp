﻿using System;
using System.Collections.Generic;

namespace ArtGallery.Models
{
    public partial class Product
    {
        public Product()
        {
            Comment = new HashSet<Comment>();
        }

        public int Id { get; set; }
        public int? ProfileId { get; set; }
        public string Title { get; set; }
        public string Descrioption { get; set; }
        public int? ImageId { get; set; }
        public string Currency { get; set; }
        public float Payment { get; set; }

        public virtual Image Image { get; set; }
        public virtual Profile Profile { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
    }
}
