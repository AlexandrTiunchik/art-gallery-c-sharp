﻿using System;
using System.Collections.Generic;

namespace ArtGallery.Models
{
    public partial class Skill
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ProfileId { get; set; }

        public virtual Profile Profile { get; set; }
    }
}
