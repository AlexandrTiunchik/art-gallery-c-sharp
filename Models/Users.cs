﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.AspNetCore.Identity;

namespace ArtGallery.Models
{
    public partial class Users : IdentityUser<int>
    {
        public string ConfirmationToken { get; set; }
        
        public string Role { get; set; }
        
        public string Avatar { get; set; }
        
        public string Status { get; set; }
        
        public Profile Profile { get; set; }
        
//        public Users()
//        {
//            Comment = new HashSet<Comment>();
//        }

//        public int Id { get; set; }
//        public string Login { get; set; }
//        public string Password { get; set; }
//        public DateTime DateRegistration { get; set; }
//        public int? AvatarId { get; set; }
//        public string Email { get; set; }
//
//        public virtual Image Avatar { get; set; }
//        public virtual ICollection<Comment> Comment { get; set; }
    }
}
