﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;


namespace ArtGallery.Models
{
    public partial class Profile
    {
//        public Profile()
//        {
//            Diy = new HashSet<Diy>();
//            News = new HashSet<News>();
//            Product = new HashSet<Product>();
//            Skill = new HashSet<Skill>();
//            SocialNetwork = new HashSet<SocialNetwork>();
//            UserContent = new HashSet<UserContent>();
//            Work = new HashSet<Work>();
//        }

        [Key, ForeignKey("Users")]
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Gender { get; set; }
        public DateTime Birthday { get; set; }
        
        public List<Comment> Comments { get; set; }
        public Users Users { get; set; }

        //public virtual ICollection<Diy> Diy { get; set; }
        //public virtual ICollection<News> News { get; set; }
        //public virtual ICollection<Product> Product { get; set; }
        //public virtual ICollection<Skill> Skill { get; set; }
        //public virtual ICollection<SocialNetwork> SocialNetwork { get; set; }
       // public virtual ICollection<UserContent> UserContent { get; set; }
        //public virtual ICollection<Work> Work { get; set; }
    }
}
