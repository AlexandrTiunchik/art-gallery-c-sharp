namespace ArtGallery.Contracts.V1
{
    public static class ApiRoutes
    {
        public const string Root = "api";
        
        public const string Version = "v1";

        public const string Base = Root + "/" + Version;
        
        public static class News
        {
            public const string GetAll = Base + "/news";

            public const string Get = Base + "/news/{newsId}";
            
            public const string Create = Base + "/news";

            public const string Update = Base + "/news";

            public const string Delete = Base + "/news/{newsId}";

            public const string GetNewsList = Base + "/news/get-news-list";

            public const string GetNewsStatistic = Base + "/news/statistic";
            
            public const string AddComment = Base + "/news/comment";
            
            public const string GetComments = Base + "/news/comment/{newsId}";
            
            public const string DeleteComment = Base + "/news/comment/{commentId}";
            
            public const string UpdateComment = Base + "/news/comment/{commentId}";
        }
        
        public static class Identity
        {
            public const string Login = Base + "/identity/login";
            
            public const string Register = Base + "/identity/register";

            public const string EmailConfirmation = Base + "/identity/email-сonfirmation/{emailId}";
        }
        
        public static class Application
        {
            public const string GlobalSearch = Base + "/application/search";
        }
        
        public static class User
        {
            public const string GetAuthorizedUser = Base + "/user/auth-user";

            public const string GetUserList = Base + "/user/get-user-list";
        }
    }
}