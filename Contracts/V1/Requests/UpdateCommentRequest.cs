namespace ArtGallery.Contracts.V1.Requests
{
    public class UpdateCommentRequest
    {
        public string Message { get; set; }
    }
}