namespace ArtGallery.Contracts.V1.Requests
{
    public class UpdatePostRequest
    {
        public string Name { get; set; }
    }
}