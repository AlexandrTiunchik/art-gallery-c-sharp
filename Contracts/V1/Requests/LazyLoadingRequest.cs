namespace ArtGallery.Contracts.V1.Requests
{
    public class LazyLoadingRequest
    {
        public int Skip { get; set; }
        
        public int Take { get; set; }
        
        public string SortField { get; set; }
        
        public int SortOrder { get; set; }
        
        public string SearchValue { get; set; }
    }
}