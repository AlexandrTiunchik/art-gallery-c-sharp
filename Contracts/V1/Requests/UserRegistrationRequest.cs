namespace ArtGallery.Contracts.V1.Requests
{
    public class UserRegistrationRequest
    {
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}