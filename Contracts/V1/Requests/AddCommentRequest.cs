using System;

namespace ArtGallery.Contracts.V1.Requests
{
    public class AddCommentRequest
    {
        public int ContentId { get; set; }
        
        public string Message { get; set; }
        
        public DateTime DateTime { get; set; }
    }
}