using System;

namespace ArtGallery.Contracts.V1.Requests
{
    public class CreateNewsRequest
    {
        public string Title { get; set; }
        
        public string Text { get; set; }
        
        public string MainImage { get; set; }
    }
}