namespace ArtGallery.Contracts.V1.Requests
{
    public class UpdateNewsRequest
    {
        public int Id { get; set; }
        
        public string Title { get; set; }
        
        public string Text { get; set; }
    
        public string MainImage { get; set; }
    }
}