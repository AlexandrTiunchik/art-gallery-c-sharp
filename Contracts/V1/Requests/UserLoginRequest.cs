namespace ArtGallery.Contracts.V1.Requests
{
    public class UserLoginRequest
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}