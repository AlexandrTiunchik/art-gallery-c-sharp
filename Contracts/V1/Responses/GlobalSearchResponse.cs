using System.Collections.Generic;
using ArtGallery.Models;

namespace ArtGallery.Contracts.V1.Responses
{
    public class GlobalSearchResponse
    {
        public List<News> NewsList { get; set; }
        
        public List<Users> UsersList { get; set; }
    }
}