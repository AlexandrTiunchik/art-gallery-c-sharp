namespace ArtGallery.Contracts.V1.Responses
{
    public class AuthorizedUserInfoResponse
    {
        public string Role { get; set; }
        
        public int Id { get; set; }
        
        public string UserName { get; set; }
        
        public string Email { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public string Avatar { get; set; }
        
        public string Status { get; set; }
    }
}
