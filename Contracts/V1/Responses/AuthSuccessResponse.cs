namespace ArtGallery.Contracts.V1.Responses
{
    public class AuthSuccessResponse
    {
        public string Token { get; set; }
        
        public string Message { get; set; }
    }
}