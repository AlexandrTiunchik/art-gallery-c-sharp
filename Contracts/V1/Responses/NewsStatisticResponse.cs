namespace ArtGallery.Contracts.V1.Responses
{
    public class NewsStatisticResponse
    {
        public int NewsCount { get; set; }
        public int musicNewsCount { get; set; }
        public int filmNewsCount { get; set; }
        public int artNewsCount { get; set; }
        public int otherNewsCount { get; set; }
    }
}