using System.Collections.Generic;

namespace ArtGallery.Domain
{
    public class AuthenticationResult : RequestResult
    {
        public string Token { get; set; }
    }
}
