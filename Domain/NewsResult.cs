using System.Collections.Generic;
using ArtGallery.Models;

namespace ArtGallery.Domain
{
    public class NewsResult : RequestResult
    {
        public List<News> News { get; set; } 
        
        public News SingleNews { get; set; }
    }
}
