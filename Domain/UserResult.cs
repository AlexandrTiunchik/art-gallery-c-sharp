using System.Collections.Generic;
using ArtGallery.Models;

namespace ArtGallery.Domain
{
    public class UserResult : RequestResult
    {
        public List<Users> UserList { get; set; }
        
        public Users SingleUser { get; set; }
    }
}
