using System.Collections.Generic;
using ArtGallery.Models;

namespace ArtGallery.Domain
{
    public class CommentsResult : RequestResult
    {
        public List<Comment> Comments { get; set; }
    }
}