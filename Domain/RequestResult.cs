using System.Collections.Generic;

namespace ArtGallery.Domain
{
    public class RequestResult
    {
        public bool Success { get; set; }
        
        public IEnumerable<string> Errors { get; set; }
        
        public string Message { get; set; }
        
        public int CollectionArrayLength { get; set; }
    }
}
