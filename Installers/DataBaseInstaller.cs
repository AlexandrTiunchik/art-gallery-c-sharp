using ArtGallery.Context;
using ArtGallery.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ArtGallery.Installers
{
    public class DataBaseInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection service, IConfiguration configuration)
        {
            /**
             * Entity Framework Service 
             */
            service.AddEntityFrameworkNpgsql().AddDbContext<ApplicationContext>(opt =>
                opt.UseNpgsql(configuration.GetConnectionString("PostgresApiConnection")));

            service.AddDefaultIdentity<Users>()
                .AddEntityFrameworkStores<ApplicationContext>();
        }
    }
}