﻿using ArtGallery.Models;
using Microsoft.EntityFrameworkCore;

namespace ArtGallery.Context
{
    public partial class ApplicationContext : DbContext
    {
        public ApplicationContext()
        {
        }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }
        
        public virtual DbSet<Users> Users { get; set; }
        
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        
        public virtual DbSet<Comment> Comments { get; set; }
        
//        public virtual DbSet<Diy> Diy { get; set; }
//        public virtual DbSet<Image> Image { get; set; }
//        public virtual DbSet<Product> Product { get; set; }
//        public virtual DbSet<Profile> Profile { get; set; }
//        public virtual DbSet<Skill> Skill { get; set; }
//        public virtual DbSet<SocialNetwork> SocialNetwork { get; set; }
//        public virtual DbSet<UserContent> UserContent { get; set; }
//        public virtual DbSet<Work> Work { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql(
                    "User ID=eumljhbandlchn;Password=2a5ac93aff09d738a20a2a099527a27a386f288a632f4a844ca9edeeef5a58a9;Host=ec2-54-228-246-214.eu-west-1.compute.amazonaws.com;Port=5432;Database=d1g16c4483fuor;Pooling=true;Use SSL Stream=True;SSL Mode=Require;TrustServerCertificate=True;");
            }
        }
    }
}
