using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ArtGallery.Context;
using ArtGallery.Domain;
using ArtGallery.Models;
using ArtGallery.Options;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using MimeKit;

namespace ArtGallery.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<Users> _userManager;
        private readonly JwtSettings _jwtSettings;
        private readonly ApplicationContext _dbContext;
        private readonly IHostingEnvironment _appEnvironment;

        public IdentityService(
            UserManager<Users> userManager,
            JwtSettings jwtSettings,
            ApplicationContext dbContext,
            IHostingEnvironment appEnvironment
            )
        {
            _userManager = userManager;
            _jwtSettings = jwtSettings;
            _dbContext = dbContext;
            _appEnvironment = appEnvironment;

        }
        public async Task<AuthenticationResult> RegisterAsync(string email, string password, string name)
        {
            var user = await _userManager.FindByEmailAsync(email);
            
            if (user != null)
            {
                return new AuthenticationResult
                {
                    Errors = new[] {"User already exists"}
                };
            }

            string emailConfirmationToken = Guid.NewGuid().ToString("N");

            var newUser = new Users
            {
                Email = email,
                UserName = name,
                ConfirmationToken = emailConfirmationToken,
                Role = "user",
                Avatar = "https://icon-library.net/images/avatar-icon-png/avatar-icon-png-8.jpg",
                Status = "inactive"
            };

            var createUser = await _userManager.CreateAsync(newUser, password);

            if (!createUser.Succeeded)
            {
                return new AuthenticationResult
                {
                    Errors = createUser.Errors.Select(x => x.Description)
                };
            }

            var createProfile = new Profile {UserId = newUser.Id};
            await _dbContext.Profiles.AddAsync(createProfile);
            await _dbContext.SaveChangesAsync();
            
            //var content = await File.ReadAllTextAsync(Path.Combine(_appEnvironment.ContentRootPath, "Content/EmailConfirmationLetter.html"));
            
            var newContent = content.Replace("REPLACE", $"https://webapp-190806220116.azurewebsites.net/api/v1/identity/email-сonfirmation/{emailConfirmationToken}");
            
            await SendEmailAsync(email, "Confirm your account", newContent);

            return new AuthenticationResult
            {
                Success = true,
                Message = "Please check email"
            };

        }
        
        /**
         * Login
         */
        public async Task<AuthenticationResult> LoginAsync(string name, string password)
        {
            var user = await _userManager.FindByNameAsync(name);
            
            if (user == null)
            {
                return new AuthenticationResult
                {
                    Errors = new[] {"User does not exists"}
                };
            }

            if (!user.EmailConfirmed)
            {
                return new AuthenticationResult
                {
                    Errors = new[] {"User account not verified"}
                };
            }
            
            var userHasValidPassword = await _userManager.CheckPasswordAsync(user, password);

            if (!userHasValidPassword)
            {
                return new AuthenticationResult
                {
                    Errors = new[] {"User/password combination is wrong"}
                };
            }
            
            return GenerateAuthenticationResultForUser(user);
        }

        private AuthenticationResult GenerateAuthenticationResultForUser(Users newUser)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSettings.Secret);
            IdentityOptions identityOptions = new IdentityOptions();
            
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(identityOptions.ClaimsIdentity.RoleClaimType, newUser.Role), 
                    new Claim(JwtRegisteredClaimNames.UniqueName, newUser.UserName),
                    new Claim(JwtRegisteredClaimNames.Email, newUser.Email),
                    new Claim("id", newUser.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                }),
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials =
                    new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);


            return new AuthenticationResult
            {
                Success = true,
                Token = tokenHandler.WriteToken(token)
            };
        }

        public async Task EmailConfirmation(string emailTokenId)
        {
            Users user = await _dbContext.Users
                .FirstOrDefaultAsync(users => users.ConfirmationToken == emailTokenId);

            if (user != null)
            {
                user.ConfirmationToken = null;
                user.EmailConfirmed = true;
                user.Role = "user";
                user.Status = "active";
                await _dbContext.SaveChangesAsync();
            }
        }

        /**
         * Email authentication
         */
        public async Task<string> SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("ArtGallery", "aleksandrtiunchik8s@gmail.com"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587, false);
                await client.AuthenticateAsync("aleksandrtiunchik8s@gmail.com", "NapolOrde_97");
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }

            return "Please check email";
        }

        private string content = "<!doctype html>\n<head>\n    <meta name=\"viewport\" content=\"width=device-width\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <title>ArtGallery</title>\n\n    <style>\n        body {\n            display: flex;\n            justify-content: center;\n            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;\n            padding: 20px;\n        }\n\n        .content {\n            max-width: 400px;\n            padding: 20px;\n        }\n\n        h1 {\n            margin-top: 0;\n            text-align: center;\n            font-weight: 800;\n        }\n\n        a {\n            background-color: greenyellow;\n            text-decoration: none;\n            padding: 10px;\n            font-size: 20px;\n            color: black;\n            border-radius: 5px;\n        }\n\n        .message {\n            font-size: 18px;\n            margin-bottom: 30px;\n        }\n\n        .hi {\n            font-size: 25px;\n        }\n\n        img {\n            width: 100%;\n        }\n\n    </style>\n</head>\n\n<body>\n<div class=\"content\">\n    <h1>ArtGallery</h1>\n    <img src=\"https://cdn.shopify.com/s/files/1/1751/8993/files/brush_2000x.jpg?v=1550488241\">\n    <div class=\"hi\">Hi,</div>\n    <p class=\"message\">Please complete your account registration by confirming your email address.</p>\n    <a href=\"REPLACE\" target=\"_blank\">VERIFY EMAIL</a>\n</div>\n</body>\n";
    }
    
}
