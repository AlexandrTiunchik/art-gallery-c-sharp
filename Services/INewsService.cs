using System.Threading.Tasks;
using ArtGallery.Contracts.V1.Requests;
using ArtGallery.Contracts.V1.Responses;
using ArtGallery.Domain;

namespace ArtGallery.Services
{
    public interface INewsService
    {
        Task<NewsResult> GetAllNewsAsync();

        Task<NewsResult> GetNewsListAsync(LazyLoadingRequest lazyLoading);

        Task<NewsResult> GetNewsAsync(int newsId);

        Task<NewsResult> CreateNewsAsync(CreateNewsRequest newsRequest);

        Task<NewsResult> DeleteNewsAsync(int newsId);

        Task<NewsResult> UpdateNewsAsync(UpdateNewsRequest newsRequest);

        Task<NewsStatisticResponse> GetNewsStatisticAsync();

        Task<CommentsResult> AddCommentAsync(AddCommentRequest comment, int userId);

        Task<CommentsResult> GetCommentsAsync(int newsId);
        
        Task<CommentsResult> DeleteCommentsAsync(int commentId, int userId);

        Task<CommentsResult> UpdateCommentsAsync(int commentId, int userId, UpdateCommentRequest commentRequest);
    }
}
