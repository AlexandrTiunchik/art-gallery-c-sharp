using System.Threading.Tasks;
using ArtGallery.Contracts.V1.Requests;
using ArtGallery.Contracts.V1.Responses;
using ArtGallery.Domain;

namespace ArtGallery.Services
{
    public interface IUserService
    {
        Task<AuthorizedUserInfoResponse> GetAuthorizedUserAsync(int userId);

        Task<UserResult> GetUserListAsync(LazyLoadingRequest lazyLoading);
    }
}
