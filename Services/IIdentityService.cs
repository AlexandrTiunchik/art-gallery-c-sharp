using System.Threading.Tasks;
using ArtGallery.Domain;

namespace ArtGallery.Services
{
    public interface IIdentityService
    {
        Task<AuthenticationResult> RegisterAsync(string email, string password, string name);

        Task<AuthenticationResult> LoginAsync(string name, string password);

        Task EmailConfirmation(string emailToken);
    }
}
