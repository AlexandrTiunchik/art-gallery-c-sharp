using System.Threading.Tasks;
using ArtGallery.Contracts.V1.Responses;

namespace ArtGallery.Services
{
    public interface IApplicationService
    {
        Task<GlobalSearchResponse> GlobalSearchAsync(string searchValue);
    }
}