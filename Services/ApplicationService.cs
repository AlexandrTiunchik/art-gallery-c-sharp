using System.Linq;
using System.Threading.Tasks;
using ArtGallery.Context;
using ArtGallery.Contracts.V1.Responses;
using ArtGallery.Models;
using Microsoft.EntityFrameworkCore;

namespace ArtGallery.Services
{
    public class ApplicationService : IApplicationService
    {
        private readonly ApplicationContext _dbContext;

        public ApplicationService(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<GlobalSearchResponse> GlobalSearchAsync(string searchValue)
        {
            var newsList = await _dbContext.News
                .Where(x => x.Title.ToLower().Contains(searchValue.ToLower()))
                .ToListAsync();
            
            var usersList = await _dbContext.Users
                .Where(x => x.UserName.ToLower().Contains(searchValue.ToLower()))
                .ToListAsync();

            return new GlobalSearchResponse
            {
                NewsList = newsList,
                UsersList = usersList
            };
        }
    }
}