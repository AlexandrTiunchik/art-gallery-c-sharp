using System.Linq;
using System.Threading.Tasks;
using ArtGallery.Context;
using ArtGallery.Contracts.V1.Requests;
using ArtGallery.Contracts.V1.Responses;
using ArtGallery.Domain;
using Microsoft.EntityFrameworkCore;

namespace ArtGallery.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationContext _dbContext;

        public UserService(
            ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        /**
         * Get information about authorized user
         */
        public async Task<AuthorizedUserInfoResponse> GetAuthorizedUserAsync(int userId)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(users => users.Id == userId);

            var authorizedUserInfo = new AuthorizedUserInfoResponse
            {
                Avatar = user.Avatar,
                Email = user.Email,
                Id = user.Id,
                Role = user.Role,
                PhoneNumber = user.PhoneNumber,
                UserName = user.UserName,
                Status = user.Status
            };
            
            return authorizedUserInfo;
        }

        public async Task<UserResult> GetUserListAsync(LazyLoadingRequest lazyLoading)
        {
            var users = await _dbContext.Users.ToListAsync();
            var usersList = users.Skip(lazyLoading.Skip).Take(lazyLoading.Take).ToList();
            var usersLength = await _dbContext.Users.CountAsync();

            return new UserResult
            {
                UserList = usersList,
                CollectionArrayLength = usersLength
            };
        }
    }
}
