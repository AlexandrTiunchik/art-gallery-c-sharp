using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;
using ArtGallery.Context;
using ArtGallery.Contracts.V1.Requests;
using ArtGallery.Contracts.V1.Responses;
using ArtGallery.Domain;
using ArtGallery.Models;
using Microsoft.EntityFrameworkCore;

namespace ArtGallery.Services
{
    public class NewsService : INewsService
    {
        private readonly ApplicationContext _dbContext;

        public NewsService(
            ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<NewsResult> GetAllNewsAsync()
        {
            var allNews = await _dbContext.News.ToListAsync();

            return new NewsResult
            {
                News = allNews
            };
        }

        public async Task<NewsResult> GetNewsListAsync(LazyLoadingRequest lazyLoading)
        {
            List<News> news;
            
            if (!string.IsNullOrWhiteSpace(lazyLoading.SearchValue) && lazyLoading.SearchValue != null)
            {
                news = await _dbContext.News
                    .Where(x => x.Title.ToLower().Contains(lazyLoading.SearchValue.ToLower()))
                    .ToListAsync();
            }
            else
            {
                news = await _dbContext.News.ToListAsync();
            }

            List<News> sortedNews;

            if (lazyLoading.SortField == "date")
            {
                if (lazyLoading.SortOrder > 0)
                {
                    sortedNews = news.OrderByDescending(x => x.Date).ToList();
                }
                else if (lazyLoading.SortOrder < 0)
                {
                    sortedNews = news.OrderBy(x => x.Date).ToList();
                }
                else
                {
                    sortedNews = news;
                }
            }
            else if (lazyLoading.SortField == "title")
            {
                sortedNews = news.OrderBy(x => x.Title).ToList();
            }
            else
            {
                sortedNews = news;
            }
            
            var newsList = sortedNews.Skip(lazyLoading.Skip).Take(lazyLoading.Take).ToList();
            var newsLength = await _dbContext.News.CountAsync();

            return new NewsResult
            {
                News = newsList,
                CollectionArrayLength = newsLength
            };
        }

        public async Task<NewsResult> GetNewsAsync(int newsId)
        {
            var news = await _dbContext.News.FirstOrDefaultAsync(x => x.Id == newsId);

            if (news == null)
            {
                return new NewsResult
                {
                    Errors = new List<string>{"News not find"}
                };
            }
            
            return new NewsResult
            {
                SingleNews = news
            };
        }

        public async Task<NewsResult> CreateNewsAsync(CreateNewsRequest newsRequest)
        {
            var newNews = new News
            {
                Date = DateTime.Today.ToShortDateString(),
                Text = newsRequest.Text,
                Title = newsRequest.Title,
                MainImage = newsRequest.MainImage
            };

            await _dbContext.AddAsync(newNews);

            await _dbContext.SaveChangesAsync();

            return new NewsResult
            {
                SingleNews = newNews,
                Message = "News was created"
            };
        }

        public async Task<NewsResult> DeleteNewsAsync(int newsId)
        {
            var news = await _dbContext.News.FirstOrDefaultAsync(x => x.Id == newsId);
            
            if (news == null)
            {
                return new NewsResult
                {
                    Errors = new List<string>{"News not find"}
                };
            }

            _dbContext.News.Remove(news);

            await _dbContext.SaveChangesAsync();

            return new NewsResult
            {
                Message = "News was deleted",
                SingleNews = news
            };
        }

        public async Task<NewsResult> UpdateNewsAsync(UpdateNewsRequest newsRequest)
        {
            var news = await _dbContext.News.SingleOrDefaultAsync(x => x.Id == newsRequest.Id);

            if (news == null || newsRequest.Id.Equals(null))
            {
                return new NewsResult
                {
                    Errors = new[] {"News are not found"}
                };
            }
            
            _dbContext.Entry(news).CurrentValues.SetValues(newsRequest);
            await _dbContext.SaveChangesAsync();

            return new NewsResult
            {
                Success = true,
                Message = "News was successed updated",
                SingleNews = news
            };
        }

        public async Task<NewsStatisticResponse> GetNewsStatisticAsync()
        {
            var newsCount = await _dbContext.News.CountAsync();
            var musicNewsCount = await _dbContext.News.Where(x => x.NewsType == "music").CountAsync();
            var filmNewsCount = await _dbContext.News.Where(x => x.NewsType == "film").CountAsync();
            var artNewsCount = await _dbContext.News.Where(x => x.NewsType == "art").CountAsync();
            var otherNewsCount = await _dbContext.News
                .Where(x => x.NewsType != "art" && x.NewsType != "music" && x.NewsType != "film").CountAsync();

            return new NewsStatisticResponse
            {
                NewsCount = newsCount,
                artNewsCount = artNewsCount,
                filmNewsCount = filmNewsCount,
                musicNewsCount = musicNewsCount,
                otherNewsCount = otherNewsCount
            };
        }

        public async Task<CommentsResult> AddCommentAsync(AddCommentRequest request, int userId)
        {
            var news = await _dbContext.News.FirstOrDefaultAsync(x => x.Id == request.ContentId);

            var profile = await _dbContext.Profiles.FirstOrDefaultAsync(x => x.UserId == userId);

            var comment = new Comment
            {
                Message = "test",
                News = news,
                Profile = profile
            };

            await _dbContext.Comments.AddAsync(comment);

            await _dbContext.SaveChangesAsync();

            var comments = await _dbContext.News
                .Where(x1 => x1.Id == news.Id)
                .Include(x2 => x2.Comments)
                .ThenInclude(x3 => x3.Profile.Users)
                .ToListAsync();
            
            return new CommentsResult
            {
                Comments = comments[0].Comments,
                Success = true
            };
        }

        public async Task<CommentsResult> GetCommentsAsync(int newsId)
        {

            var comments = await _dbContext.News
                .Where(x1 => x1.Id == newsId)
                .Include(x2 => x2.Comments)
                .ThenInclude(x3 => x3.Profile.Users)
                .ToListAsync();

            return new CommentsResult
            {
                Comments = comments[0].Comments,
                Success = true
            };
        }

        public async Task<CommentsResult> DeleteCommentsAsync(int commentId, int userId)
        {
            var comment = await _dbContext.Comments.FirstOrDefaultAsync(x => x.Id == commentId);
            
            if (comment == null)
            {
                return new CommentsResult
                {
                    Success = false,
                    Errors = new List<string>{"Comment not found"}
                };
            }

            var userRole = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);

            if (comment.ProfileId != userId || userRole.Role != "admin")
            {
                return new CommentsResult
                {
                    Success = false,
                    Errors = new List<string>{"You have no permission for delete comment"}
                };
            }

            _dbContext.Comments.Remove(comment);

            await _dbContext.SaveChangesAsync();

            return new CommentsResult
            {
                Success = true,
                Message = "Comment was successed deleted",
            };
        }

        public async Task<CommentsResult> UpdateCommentsAsync(int commentId, int userId, UpdateCommentRequest commentRequest)
        {
            var comment = await _dbContext.Comments.FirstOrDefaultAsync(x => x.Id == commentId);
            
            if (comment == null)
            {
                return new CommentsResult
                {
                    Success = false,
                    Errors = new List<string>{"Comment not found"}
                };
            }

            var userRole = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);

            if (comment.ProfileId != userId || userRole.Role != "admin")
            {
                return new CommentsResult
                {
                    Success = false,
                    Errors = new List<string>{"You have no permission for delete comment"}
                };
            }
            
            _dbContext.Entry(comment).CurrentValues.SetValues(commentRequest);
            
            await _dbContext.SaveChangesAsync();

            return new CommentsResult
            {
                Success = true,
                Message = "Comment was successed updated",
            };
        }
    }
}
