using System.Threading.Tasks;
using ArtGallery.Contracts.V1;
using ArtGallery.Contracts.V1.Requests;
using ArtGallery.Contracts.V1.Responses;
using ArtGallery.Services;
using Microsoft.AspNetCore.Mvc;

namespace ArtGallery.Controllers.V1
{
    public class ApplicationController : Controller
    {
        private readonly IApplicationService _applicationService;

        public ApplicationController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }
        
        [HttpGet(ApiRoutes.Application.GlobalSearch)]
        public async Task<GlobalSearchResponse> GlobalSearch(string search)
        {
            var searchResult = await _applicationService.GlobalSearchAsync(search);

            return searchResult;
        }
    }
}