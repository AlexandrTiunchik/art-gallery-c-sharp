using System;
using System.Threading.Tasks;
using ArtGallery.Contracts.V1;
using ArtGallery.Contracts.V1.Requests;
using ArtGallery.Contracts.V1.Responses;
using ArtGallery.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;


namespace ArtGallery.Controllers.V1
{
    public class IdentityController : Controller
    {
        private readonly IIdentityService _idendentityService;
        
        private readonly IHostingEnvironment _appEnvironment;
        
        public IdentityController(IIdentityService identityService,
            IHostingEnvironment appEnvironment)
        {
            _idendentityService = identityService;
            _appEnvironment = appEnvironment;
        }

        /**
         * Register route
         */
        [HttpPost(ApiRoutes.Identity.Register)]
        public async Task<IActionResult> Register([FromBody] UserRegistrationRequest request)
        {
            var authResponse = await _idendentityService.RegisterAsync(request.Email, request.Password, request.Login);

            if (!authResponse.Success)
            {
                return BadRequest(new AuthFailedResponse
                {
                    Errors = authResponse.Errors
                });
            }
            
            return Ok(new AuthSuccessResponse
            {
                Message = "Registration successfully completed. Please confirm account"
            });
        }
        
        /**
         * Login route
         */
        [HttpPost(ApiRoutes.Identity.Login)]
        public async Task<IActionResult> Login([FromBody] UserLoginRequest request)
        {
            var authResponse = await _idendentityService.LoginAsync(request.Login, request.Password);

            if (!authResponse.Success)
            {
                return BadRequest(new AuthFailedResponse
                {
                    Errors = authResponse.Errors
                });
            }
            
            return Ok(new AuthSuccessResponse
            {
                Token = authResponse.Token
            });
        }

        [HttpGet(ApiRoutes.Identity.EmailConfirmation)]
        public async Task<IActionResult> EmailConfirmation(string emailId)
        {
            await _idendentityService.EmailConfirmation(emailId);
            
            //var content = await System.IO.File.ReadAllTextAsync(Path.Combine(_appEnvironment.ContentRootPath, "Content/EmailConfirmation.html"));

            var content = "<h1><a href='http://localhost:4200/authorization'><a/></h1>";
            
            return new ContentResult()
            {
                Content = content,
                ContentType = "text/html"
            };
        }
    }
}
