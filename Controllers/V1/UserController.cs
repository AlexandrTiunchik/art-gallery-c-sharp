using System;
using System.Linq;
using System.Threading.Tasks;
using ArtGallery.Contracts.V1;
using ArtGallery.Contracts.V1.Requests;
using ArtGallery.Contracts.V1.Responses;
using ArtGallery.Domain;
using ArtGallery.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ArtGallery.Controllers.V1
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(
            IUserService userService)
        {
            _userService = userService;
        }
        
        [Authorize]
        [HttpGet(ApiRoutes.User.GetAuthorizedUser)]
        public async Task<AuthorizedUserInfoResponse> GetAuthorizedUser()
        {
            var userId = HttpContext.User.Claims.Where(x => x.Type == "id").SingleOrDefault();
            var user = await _userService.GetAuthorizedUserAsync(Convert.ToInt32(userId.Value));

            return user;
        }

        [Authorize(Roles = "admin")]
        [HttpPost(ApiRoutes.User.GetUserList)]
        public async Task<UserResult> GetUserList([FromBody] LazyLoadingRequest lazyLoading)
        {
            var userResult = await _userService.GetUserListAsync(lazyLoading);

            return userResult;
        }
    }
}
