using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ArtGallery.Contracts.V1;
using ArtGallery.Contracts.V1.Requests;
using ArtGallery.Contracts.V1.Responses;
using ArtGallery.Domain;
using ArtGallery.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ArtGallery.Controllers.V1
{
    public class NewsController : Controller
    {
        private readonly INewsService _newsService;

        public NewsController(
            INewsService newsService)
        {
            _newsService = newsService;
        }

        /**
         * Get all news
         */
        [HttpGet(ApiRoutes.News.GetAll)]
        public async Task<NewsResult> GetAllNews()
        {
            var newsResult = await _newsService.GetAllNewsAsync();

            return newsResult;
        }
        
        /**
         * Get list of news
         */
        [HttpPost(ApiRoutes.News.GetNewsList)]
        public async Task<NewsResult> GetNewsList([FromBody] LazyLoadingRequest lazyLoading)
        {
            var newsResult = await _newsService.GetNewsListAsync(lazyLoading);

            return newsResult;
        }

        /**
         * Get single news
         */
        [HttpGet(ApiRoutes.News.Get)]
        public async Task<NewsResult> GetNews(int newsId)
        {
            var newsResult = await _newsService.GetNewsAsync(newsId);

            return newsResult;
        }

        /**
         * Create news
         */
        [Authorize(Roles = "admin")]
        [HttpPost(ApiRoutes.News.Create)]
        public async Task<NewsResult> CreateNews([FromBody] CreateNewsRequest request)
        {
            var newsResult = await _newsService.CreateNewsAsync(request);

            return newsResult;
        }

        /**
         * Remove news
         */
        [Authorize(Roles = "admin")]
        [HttpDelete(ApiRoutes.News.Delete)]
        public async Task<NewsResult> DeleteNews(int newsId)
        {
            var newsResult = await _newsService.DeleteNewsAsync(newsId);

            return newsResult;
        }

        [Authorize(Roles = "admin")]
        [HttpPut(ApiRoutes.News.Update)]
        public async Task<NewsResult> UpdateNews([FromBody] UpdateNewsRequest request)
        {
            var newsResult = await _newsService.UpdateNewsAsync(request);

            return newsResult;
        }

        [HttpGet(ApiRoutes.News.GetNewsStatistic)]
        public async Task<NewsStatisticResponse> GetNewsStatistic()
        {
            var newsStatistic = await _newsService.GetNewsStatisticAsync();

            return newsStatistic;
        }
        
        [Authorize]
        [HttpPost(ApiRoutes.News.AddComment)]
        public async Task<CommentsResult> AddComment([FromBody] AddCommentRequest request)
        {
            var userId = HttpContext.User.Claims.Where(x => x.Type == "id").SingleOrDefault();
            var result = await _newsService.AddCommentAsync(request, Convert.ToInt32(userId.Value));

            return result;
        }

        [HttpGet(ApiRoutes.News.GetComments)]
        public async Task<CommentsResult> GetComments(int newsId)
        {
            var result = await _newsService.GetCommentsAsync(newsId);

            return result;
        }

        [Authorize(Roles = "admin, user")]
        [HttpDelete(ApiRoutes.News.DeleteComment)]
        public async Task<CommentsResult> DeleteComment(int commentId)
        {
            var userId = HttpContext.User.Claims.Where(x => x.Type == "id").SingleOrDefault();

            var result = await _newsService.DeleteCommentsAsync(commentId, Convert.ToInt32(userId.Value));

            return result;
        }
        
        [Authorize(Roles = "admin, user")]
        [HttpPut(ApiRoutes.News.UpdateComment)]
        public async Task<CommentsResult> UpdateComment([FromBody] UpdateCommentRequest request, int commentId)
        {
            var userId = HttpContext.User.Claims.Where(x => x.Type == "id").SingleOrDefault();

            var result = await _newsService.UpdateCommentsAsync(commentId, Convert.ToInt32(userId.Value), request);

            return result;
        }
    }
}
